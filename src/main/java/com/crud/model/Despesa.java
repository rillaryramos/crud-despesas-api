package com.crud.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Despesa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private BigDecimal valor;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate data;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private TipoDespesa tipoDespesa;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private FormaPagamento formaPagamento;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private TipoReembolso tipoReembolso;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Projeto projeto;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Cliente cliente;

    @Size(max = 500)
    private String observacao;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValor() {
        BigDecimal valorDefault = new BigDecimal(0.00);
        if(valor.compareTo(valorDefault) != 0) {
            String value = valor.toString();
            value = value.replaceAll("\\.", ",");
            return value;
        }else {
            return null;
        }
    }

    public void setValor(String valor) {
        if(valor != null) {
            valor = valor.replace("R$ ", "");
            valor = valor.replaceAll("\\.", "");
            valor = valor.replaceAll(",", ".");
            BigDecimal valorBigDecimal = new BigDecimal(valor);
            this.valor = valorBigDecimal;
        }
        else {
            this.valor = null;
        }
    }

    public String getData() {
        return data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public void setData(String data) {
        if(data != null) {
            DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            this.data = LocalDate.parse(data, formatador);
        }
    }

    public TipoDespesa getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(TipoDespesa tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public TipoReembolso getTipoReembolso() {
        return tipoReembolso;
    }

    public void setTipoReembolso(TipoReembolso tipoReembolso) {
        this.tipoReembolso = tipoReembolso;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Despesa despesa = (Despesa) o;
        return Objects.equals(id, despesa.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
