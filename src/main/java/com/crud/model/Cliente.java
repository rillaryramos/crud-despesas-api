package com.crud.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Cliente {

    CLIENTE1("CLIENTE1"),
    CLIENTE2("CLIENTE2");

    private final String descricao;

    private Cliente(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
