package com.crud.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoDespesa {

    ALIMENTACAO("ALIMENTACAO"),
    ESTACIONAMENTO("ESTACIONAMENTO"),
    TAXI("TAXI");

    private final String descricao;

    private TipoDespesa(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
