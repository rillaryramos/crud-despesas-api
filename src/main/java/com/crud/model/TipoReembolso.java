package com.crud.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoReembolso {

    ADIANTAMENTO("ADIANTAMENTO"),
    NAOREEMBOLSAVEL("NAOREEMBOLSAVEL"),
    REEMBOLSAVEL("REEMBOLSAVEL");

    private final String descricao;

    private TipoReembolso(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
