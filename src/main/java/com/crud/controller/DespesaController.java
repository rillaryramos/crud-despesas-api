package com.crud.controller;

import com.crud.event.RecursoCriadoEvent;
import com.crud.model.Despesa;
import com.crud.repository.DespesaRepository;
import com.crud.service.DespesaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/despesas")
public class DespesaController {

    @Autowired
    private DespesaRepository despesaRepository;

    @Autowired
    private DespesaService despesaService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @PostMapping
    public ResponseEntity<Despesa> salvar(@Valid @RequestBody Despesa despesa, HttpServletResponse response){
        Despesa despesaSalva = despesaRepository.save(despesa);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, despesaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(despesaSalva);
    }

    @GetMapping
    public List<Despesa> listarDespesas(){
        return despesaRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Despesa> buscarDespesaPorId(@PathVariable("id") Long id){
        return ResponseEntity.ok(despesaRepository.findById(id).get());
    }

    @PutMapping
    public ResponseEntity<Despesa> editar(@Valid @RequestBody Despesa despesa){
        Despesa despesaSalva = despesaService.editar(despesa);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(despesaSalva);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long id) {
        despesaRepository.deleteById(id);
    }

}
