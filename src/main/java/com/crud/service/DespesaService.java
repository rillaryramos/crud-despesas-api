package com.crud.service;

import com.crud.model.Despesa;
import com.crud.repository.DespesaRepository;
import com.crud.service.exception.IdInvalidoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DespesaService {

    @Autowired
    private DespesaRepository despesaRepository;

    public Despesa editar(Despesa despesa){
        if(despesa.getId() == null || !despesaRepository.findById(despesa.getId()).isPresent()) {
            throw new IdInvalidoException();
        }
        return despesaRepository.save(despesa);
    }
}
